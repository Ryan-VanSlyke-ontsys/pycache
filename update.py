import testSuite as rt
from ref import prompts, checkProcessByName as cp, jsonTools as jt # Customs
from types import SimpleNamespace
import subprocess, os, json

cp.check_pageant()

CONFIG = "./ref/zConfig.txt"
NAMESPACEDIR = "C:\\InterSystems\\Cache\\CSP"
clean = ''
confirm = ''
namespace = ''
willRunTests = None

# Create a dictionary from JSON file as a SimpleNamspace so the fields can be referred to with dot notation
jsonFields = SimpleNamespace(**(jt.read_json_to_dict(CONFIG)))
# If the repository directory is empty, ask for one. Use the default if left blank.
if (jsonFields.RepositoryDirectory == ''):
    jsonFields.RepositoryDirectory = prompts.get_with_default('Enter your repository directory for the config (C:\\repos): ', 'C:\repos')
    # vars converts SimpleNameSpace back to a dict so it's JSON serializable
    jt.write_dict_to_json(vars(jsonFields),CONFIG)

# Get repo dir from config file
jsonFields = SimpleNamespace(**(jt.read_json_to_dict(CONFIG)))
repositoryDirectory = repr(jsonFields.RepositoryDirectory).replace('\\','\\\\').replace("'",'')
willTestDefault = jsonFields.WillTestDefault

import warnings
warnings.simplefilter('ignore', category=UserWarning)

# Get namespace, and branch to update to
namespaceFound = False
while (namespaceFound == False):
        namespace = prompts.force_get("Enter a namespace: ").upper()
        # Check whether the entered namespace exists in the default directory. If it does, ask for another namespace.
        for item in os.listdir(NAMESPACEDIR):
            if os.path.isdir(os.path.join(NAMESPACEDIR, item)):
                if (item.upper() == namespace):
                    namespaceFound = True
                    break
        if (namespaceFound == False):
            print(f'{namespace} not found in {NAMESPACEDIR}')

defaultToBranch = os.popen(f'cd {repositoryDirectory}\\{namespace} && hg branch').read().replace('\n','')
branch = prompts.get_with_default(f"Enter a branch to update to ({defaultToBranch}): ", defaultToBranch)

# Determine whether user wants to run a clean update
while (clean != 'Y' and clean != 'N'):
    clean = prompts.get_with_default(f'Clean? (N): ', 'N').upper()
if (clean == 'N'):
    clean = ''
    willCleanString = ''
elif (clean == "Y"):
    clean = "-c "
    willCleanString = ' and clean'

willRunTests = prompts.get_with_default(f"Run test suite after updating? ({willTestDefault}): ", willTestDefault)

# Confirm that the user wants to run the update since hg cache cannot be cancelled
while (confirm != 'Y'):
    confirm = prompts.force_get(f'Update {namespace} to {branch}{willCleanString}? Cancelling before the process is complete may render the namespace unusable: ').upper()
    if (confirm == 'N'):
        exit()

# If the user confirms, run the update and print the result
if (confirm == 'Y'):
    result = os.system(f'cd {repositoryDirectory}\\{namespace} && hg pull && hg update {clean}{branch}')
    if (result == 0):
        print('\nUpdate successful')
        if (willRunTests.upper() == "Y" ):
            print('\nRunning test suite')
            rt.run_tests(namespace)
    else: print(f'\nUpdate failed with status: {str(result)}')