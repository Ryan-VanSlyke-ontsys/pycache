
Need to fix:
File is required to be edited in borderless for the automatic debugger.

Apparently the studio window can't be minimized if you want to access it for debug attacher? Check link:
https://pywinauto.readthedocs.io/en/latest/code/pywinauto.controls.hwndwrapper.html#pywinauto.controls.hwndwrapper.DialogWrapper.is_in_taskbar

Refactor actions into modules

Create hotkey for running debug attacher

Create docs for initial setup, then script to copy key, run setup, create OSSYS using selenium, create initial namespace, and import mapping for it, run the mappings command, and maybe restart cache.