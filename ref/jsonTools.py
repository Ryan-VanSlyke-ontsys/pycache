import json
def read_json_to_dict(filePath):
    with open(filePath) as jsonFile:
        data = json.load(jsonFile)
        dic = {}
        for i in data:
            dic[i] = data[i]
    return dic
def write_dict_to_json(dic, filePath):
    with open(filePath, 'w') as out:
        json.dump(dic, out, indent=2)
    