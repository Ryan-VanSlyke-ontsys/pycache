import json
#Create default json file
config = {
    'RepositoryDirectory': '', # Directory where repositories will be cloned
    'Auto': 'true', # Automatically create namespaces. Toggle by opening zToggleAuto.bat or by running 'python tog.py' in the project directory
    'LastNamespace': 1, # Namespace to start scanning at 
    'DefaultToBranch': 'default', # Branch to clone by default if the user leaves the branch prompt empty
    'WillTestDefault': "Y" # Whether tests will be run if the user leaves the testing prompt empty (Y/N)
}
with open('zConfig.txt', 'w') as outfile:
    json.dump(config, outfile, indent=2)