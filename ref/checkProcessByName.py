import subprocess

def print_proc():
    call = 'TASKLIST', '/FO', 'CSV'
    output = subprocess.check_output(call)
    # get rid of extra " and split into lines
    output = subprocess.check_output(call).decode()
    output = output.replace('"', '').split('\r\n')
    keys = output[0].split(',')
    proc_list = [i.split(',') for i in output[1:] if i]
    # make dict with proc names as keys and dicts with the extra nfo as values
    proc_dict = dict( [( i[0], dict(zip(keys[1:], i[1:])) ) for i in proc_list] )

def is_running(process_name):
    call = 'TASKLIST', '/FI', 'imagename eq %s' % process_name
    # use buildin check_output right away
    output = subprocess.check_output(call).decode()
    # check in last line for process name
    last_line = output.strip().split('\r\n')[-1]
    # because Fail message could be translated
    return last_line.lower().startswith(process_name.lower())

def check_pageant():
    while (is_running('pageant.exe') is False):
        input('Start Pageant and add a private key')