# Create namespace
''' Using pywinauto
# Create namespace and database through sql command in cache
pyperclip.copy("do $SYSTEM.SQL.Execute(\"CREATE DATABASE "+namespace+" ON DIRECTORY 'C:\InterSystems\Cache\CSP\\"+namespace+"'\")")
cterm.type_keys('+{INS}')
# Wait for paste to wrap in CTerm
time.sleep(0.25)
cterm.type_keys('{ENTER}')
'''

# Set FACS mappings
''' Using pywinauto
pyperclip.copy('zn "'+namespace+'"')
cterm.type_keys('+{INS} {ENTER}')
pyperclip.copy('do Set2010FacsMappings^osCacheMapping')
cterm.type_keys('+{INS}')
cterm.type_keys('{ENTER}')
'''

# Run test suite
''' Using pywinauto
app = application.Cache_Telnet
app.wait('ready')
pyperclip.copy('zn "'+namespace+'"')
app.type_keys('+{INS} {ENTER}')
pyperclip.copy('do ##class(tf.test.TestManager).Run()')
app.type_keys('+{INS} {ENTER}')
'''