DROP DATABASE TestDB RETAIN_FILES


RETAIN_FILES: Optional — If specified, the physical database files (CACHE.DAT files) will not be deleted. The default is to delete the .DAT files along with the namespace and the other database entities.