#Convert methods to getWithDefault or forceGet with params
def get_with_default(prompt,default):
    try:
        result = input(prompt)
        if (result != ''):
            return result
        else:
            return default
    except Exception as e:
        print (e)
def force_get(prompt):
    while True:
        try:
            result = input(prompt)
            if (result != ''):
                return result
        except Exception as e:
            print (e)
            continue