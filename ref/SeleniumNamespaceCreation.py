from selenium import webdriver

'''SELENIUM NAMESPACE CREATION'''

''' Explicit navigation from the start page to the namespace creation page
    driver.get("http://localhost:57772/csp/sys/UtilHome.csp")
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.find_element_by_id("item_5").click()
    driver.find_element_by_id("a_0_24").click()
    driver.find_element_by_id("a_0,0_24").click()
    driver.find_element_by_id("a_0,0,1_24").click()
    driver.find_element_by_class_name("commandButton").click()'''

'''Create namespace in browser (not tested)'''
###
## Create namespace with selenium web driver using Intersystems' web portal ## Research doing this through cache config methods
###
driver = webdriver.Chrome("chromedriver.exe") # change to be browser dependent in json for chrome, firefox)
driver.set_page_load_timeout(15)
isDbCreated = 0
isSaved = 0
while (isSaved == 0):
    try:
        # Go to namespace creation page
        driver.get("http://localhost:57772/csp/sys/mgr/%25CSP.UI.Portal.Namespace.zen")
        driver.maximize_window()
        driver.implicitly_wait(10)

        # input namespace
        driver.find_element_by_id("control_17").send_keys(namespace)
        if (isDbCreated == 0):
            # Click create new database, switch to the db wizard, input name of database
            driver.find_element_by_id("control_30").click()
            driver.switch_to.frame(driver.find_element_by_id("frame_68"))
            driver.find_element_by_id("control_9").send_keys(namespace)

            # Click browse, switch to default, switch to the directory selection dialog
            driver.find_element_by_id("control_12").click()
            driver.switch_to.default_content()
            driver.switch_to.frame(driver.find_element_by_id("frame_69"))
            # Clear the directory, input the custom directory, click okay, switch to default
            driver.find_element_by_id("control_20").clear()
            driver.find_element_by_id("control_20").send_keys("C:\\InterSystems\\Cache\\dbs\\"+namespace)
            driver.find_element_by_id("control_30").click()
            driver.switch_to.default_content()

            # Switch to the db wizard, click next, click finish
            driver.switch_to.frame(driver.find_element_by_id("frame_68"))
            driver.find_element_by_id("control_68").click()
            driver.find_element_by_id("control_72").click() #driver.find_element_by_id("control_70").click() # to click finish instead of cancel
            isDbCreated = 1
        # Select the created database from the list
        driver.switch_to.default_content()
        driver.find_element_by_id("btn_29").click()
        driver.find_element_by_xpath('//div[@id="zenModalDiv"]//a[contains(text(), "' + namespace + '")]').click()
        # Select the same database from routines
        driver.find_element_by_id("btn_38").click()
        driver.find_element_by_xpath('//div[@id="zenModalDiv"]//a[contains(text(), "' + namespace + '")]').click()
        ##Might come in handy if xpath doesn't work
            #table = driver.find_element_by_css_selector("#zenModalDiv .comboboxTable")
            #for i in table.find_elements_by_css_selector("tr td:first-of-type"):
                #if (i.text == namespace):
                    #i.click()
                    #break

        # Click save
        driver.find_element_by_id("command_btnSave").click()
        isSaved = 1

        # Create Mappings through browser?
        #driver.quit()
    except Exception as e:
        print(e)
        if (isDbCreated == 0):
            print('\nThe database was created but the namespace was not saved. Retrying. ')
quit()