# PyCache
- Install Python 3 64bit to use this tool. It's tested with version 3.8.0.
- RUN THE PYTHON INSTALLER AS ADMINISTRATOR and check add to PATH [3.8.0 Download](https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64.exe).
---
- Check the python version with `python --version`
- Run Init.bat to install the required python libraries.
---

#### Features
- createNS - Automatically creates a namespace. Make sure 'Add imported items to project' is unchecked in the import menu in Studio
- toggleAuto - Toggles the automatic namespace option for zCreateNamespace
- debug - Automatically starts and attaches to the debugging process for a test file with a break command if the file is open in Cache Studio in borderless window mode.
- testSuite - Runs the test suite for the current namespace in Cache Studio, or asks for one to be input if Studio is not open.
- update - Runs a mercurial update for a namespace and asks whether you want to run the test suite afterward.
Note:
To use the update script, your repo should be the same name as your namespace, this will be the case if you use the namespace creation tool.