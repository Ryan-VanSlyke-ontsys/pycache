from ref import jsonTools as jt # Customs
from pywinauto import Application
from types import SimpleNamespace
import json, time, pywinauto, win32clipboard, re, pyperclip, requests

import warnings
warnings.simplefilter('ignore', category=UserWarning)

# Method to run a specific test command in a certain namespace
def runTest(application, namespace, testCommand):
    cterm = application.Cache_Telnet
    cterm.wait('ready')
    pyperclip.copy(f'zn "{namespace}"')
    cterm.type_keys('+{INS}{ENTER}')
    pyperclip.copy(testCommand)
    cterm.type_keys('+{INS}{ENTER}')
    return cterm

# Connect to cache studio, backend appears to need to be UIAccess ('uia') to access Cache specific dialog windows
app = Application(backend='uia').connect(path='C:\\InterSystems\\Cache\\bin\\CStudio.exe')
studio = app.top_window()

# Parse the active tab (filename) and current namespace from the cache studio title
studioWin = studio.element_info.name
namespace = studioWin[studioWin.find("/")+1:studioWin.find("@")]
activeTab = studioWin[studioWin.find("[")+1:studioWin.find("]")]
# Prevent the mad lad from trying to debug Cache system files, or other invalid files
if ('.cls' not in activeTab):
    input('Not a valid debug file type')
    quit()

# Construct the test command from the file name
testCommand = f'do ##class({activeTab[ : -4]}).RunIt()'

# Open CTerm and run the test command for the namespace and file to hit the break point in
try:
    application = Application().connect(path='C:\\InterSystems\\Cache\\bin\\CTerm.exe')
    cterm = runTest(application, namespace, testCommand)
except pywinauto.application.ProcessNotFoundError as e:
    application = Application().start(f'C:\\InterSystems\\Cache\\bin\\CTerm.exe /console=cn_iptcp:127.0.0.1[23]')
    cterm = runTest(application, namespace, testCommand)
except Exception as e:
    print(e)

# Get the PID
def getPID():
    global processFound, processID
    # Request the currently active jobs from the Cache endpoint
    r = requests.get(url = f'http://localhost:57772/api/atelier/v1/%25SYS/jobs')
    data = r.json()['result']['content']

    # Retrieve the ID for the active document's debugging process
    for key in data:
        if (key['routine'] == activeTab.replace('.cls','')+'.1'):
            processFound = True
            processID = str(key['pid'])
            break

processFound = False
processID = ''
# Try thrice to let the processID become available for the request with a 1 second delay for each attempt after the first attempt 
count = 0
while count < 3:
    if (processFound == False):
        getPID()
        count += 1
        time.sleep(1)
    else:
    # The process was found, open the dialog window to attach to a process with Ctrl+Shift+A, and skip printing the error
        count = 4
        studio.type_keys('{VK_CONTROL down}{VK_LSHIFT down}{a down}{VK_CONTROL up}{VK_LSHIFT up}{a up}')
        pyperclip.copy(processID)
        studio.child_window(title='Attach to Process',control_type="Window").type_keys('{TAB}+{INS}{ENTER}')
# If the process is not found after 3 attempts, notify the user and exit
if (count == 3):
    input('Process not found after 3 attempts. Please ensure that the file is compiled and contains a break statement.'), quit()

# Enter the 'go' command to go to the breakpoint in CStudio
cterm.type_keys('g{ENTER}')
# Bring studio to the front
studio.set_focus()