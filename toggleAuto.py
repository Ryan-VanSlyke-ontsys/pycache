from ref import jsonTools as jt
from types import SimpleNamespace
CONFIG = './ref/zConfig.txt'

import warnings
warnings.simplefilter('ignore', category=UserWarning)

jsonFields = SimpleNamespace(**(jt.read_json_to_dict(CONFIG)))
if (jsonFields.Auto.lower() == "true"):
    jsonFields.Auto = "false"
else:
    jsonFields.Auto = "true"
jt.write_dict_to_json(vars(jsonFields),CONFIG)
