import json, os, glob, subprocess, sys, termios
from os import path
from datetime import datetime

class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

def check_status(status, msg=""):
    if (status != 0):
        print(msg)
        exit()

def get_ri(prompt):
    try:
        # Throw away input which came before the prompt is shown
        termios.tcflush(sys.stdin, termios.TCIOFLUSH)
        return raw_input(prompt)
    except KeyboardInterrupt:
        exit()
    except Exception as e:
        print(e)

def get_with_default(prompt,default):
    try:
        result = get_ri(prompt)
        if (result != ''):
            return result
        else:
            return default
    except KeyboardInterrupt:
        exit()
    except Exception as e:
        print (e)
        
def make_namespace(first_repo = False):

    if (first_repo):
        instance = get_ri('Enter the ensenv instance name to create: ')
        status = os.system('ensenv start '+instance+' --version 2017.1.3.317.0.18201 --ports 1972:1972,1878:1878,1200:1200,16715:16715,15003:15003,14001:14001,8445:8443 --time-zone US/Eastern')
        check_status(status, "Failed to start new instance with 'ensenv'")

    # Check whether an ensenv list shows 'Up'
    if (os.system('ensenv list | grep Up') != 0):
        os.system('ensenv list')
        ensenv = get_ri('Enter an ensemble instance to start: ')
        os.system('ensenv start '+ensenv)

    # If cake.json doesn't exist, create it with the desired ensenv instance
    if (not path.exists('cake.json')):
        print("\nRunning 'ensenv list':")
        os.system('ensenv list')
        instance = get_ri('Enter the ensenv instance to use: ').strip()
        
        cmd = 'echo \'{"instance": "'+instance+'", "excludeTCTests": "1", "namespace": "none"}\' >> cake.json'
        os.system(cmd)

    # Save namespace to build
    print('\nListing: ')
    os.system('ls')
    with open('cake.json') as json_file:
        data = json.load(json_file)
    print('\nCurrent namespace: '+data['namespace'])
    namespace = get_ri('\nEnter the namespace to build: ').strip()
    data['namespace'] = namespace
    with open('cake.json', 'w') as outfile:
        json.dump(data,outfile)

    just_clone = get_with_default('Would you like to clone FACS and exit? (N): ',"N").upper() == 'Y'

    # Get branch
    branch = get_ri('Enter the branch to clone or blank for an existing repo: ').strip()

    # Determine whether to use init or namespace make command
    if first_repo:
        ns_cmd = 'init'
    else:
        ns_cmd = 'namespace'

    # Get branch label
    label = get_with_default('Enter a label for your new branch, or blank to stay on the cloned branch: ','').strip()
    if (label != ''):
        labelString = ' && git checkout -b '+label
    else:
        labelString = ''

    if just_clone:
        cmd = 'git clone git@bitbucket.org:ontsys/facs.git -b '+branch+' '+namespace+' && ' if branch != '' else ''
        cmd += 'cp cake.json '+namespace
        os.system(cmd)
        exit()

    # Run tests after cloning?
    willTest = get_with_default('Run tests? (Y): ','Y').strip()
    if (willTest.upper() == 'Y'):
        willTest = ' && make dev-test'
    else:
        willTest = ''
       
    will_import = get_with_default('Run import? (Y): ',"Y")
 
    # Clone facs repo and run the make tools
    cmd = 'git clone git@bitbucket.org:ontsys/facs.git -b '+branch+' '+namespace+' && ' if branch != '' else ''
    cmd += 'cp cake.json '+namespace
    cmd += ' && cd '+namespace + labelString+' && make dev-'+ns_cmd+' && make dev-hooks'
    cmd += '&& make dev-update' if will_import == 'Y' else ''
    cmd += willTest

    prepend_commit = get_with_default('Prepend commit messages with branch name? (Y): ','Y').strip()

    #print('\ncmd: '+cmd+"\n")

    status = os.system(cmd)
    if (status == 0):
        print(bcolors.OKGREEN+'Clone and make successful'+bcolors.ENDC)
    else:
        print(bcolors.FAIL+'Clone and make failed'+bcolors.ENDC)

    #play sound

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("Completed at: " + current_time)

    if (prepend_commit == 'Y'):
        status = os.system('printf \'BRANCH_NAME=$(git symbolic-ref --short HEAD)\nif [ -n "$BRANCH_NAME" ] && [ "$BRANCH_NAME" != "master" ]; then\n    echo "$BRANCH_NAME: $(cat $1)" > $1\nfi\' >> ./'+namespace+'/.git/hooks/prepare-commit-msg')
        check_status(status, bcolors.FAIL+'Failed to add prepare-commit-msg to git hooks'+bcolors.ENDC)

    #if status == 0 and first_repo:
        #os.system("sudo apt update && sudo apt upgrade")

if __name__ == '__main__':
    make_namespace(first_repo = False)
