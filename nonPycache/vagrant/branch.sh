#!/bin/bash

for D in *; do
    [ -d "${D}" ] || continue
        echo "${D}"
	cd $D
	git branch --column
	cd ..
	#echo -e "\n" # echo with escape chars
done

exit
