#!/bin/bash

for D in *; do
    [ -d "${D}" ] || continue
        echo "${D}"
	cd $D
	git branch
	cd ..
	echo -e "\n" # echo with escape chars
done
