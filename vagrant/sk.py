import os
from nsj import make_namespace

email = raw_input("Enter BitBucket email: ")

# Generate ssh key
os.system("ssh-keygen -q -t rsa -b 4096 -C "+email+" -N '' -f ~/.ssh/id_rsa")

# Start the ssh agent
os.system('eval $(ssh-agent -s')
# Add ssh private key to the ssh agent
os.system('ssh-add ~/.ssh/id_rsa')

print('Copy this ssh key to BitBucket at https://bitbucket.org/account/settings/ssh-keys/\n')
# Display the ssh key
os.system('cat ~/.ssh/id_rsa.pub')

make_namespace(first_repo = True)
