import keyboard, pywinauto, requests
from pywinauto import Application
from lxml import html

import os, time, webbrowser, win32clipboard as wcb
import urllib3
urllib3.disable_warnings()

try:
    application = Application().connect(path='C:\\Windows\\System32\\cmd.exe')
except pywinauto.application.ProcessNotFoundError as e:
    application = Application().start('C:\\Windows\\System32\\cmd.exe')
except Exception as e:
    print(e)

global app
app = application.top_window()

def toggle_window():
    if app.has_focus():
        app.minimize()
    else:
        app.set_focus()

def field_request():
    keyboard.release('ctrl+shift+`')
    if (not app.has_focus()):
        keyboard.send('ctrl+c')
        time.sleep(0.1)
        wcb.OpenClipboard()
        time.sleep(0.1)
        clip = wcb.GetClipboardData().strip().upper()
        wcb.CloseClipboard()
        url= f'\n\nhttps://osc-mu-dev01.ontariosystems.com/csp/FACSDD/todd.DataDictionary.cls?global=&field={clip}'
        r = requests.get(url = url, verify=False) 
        tree = html.fromstring(r.content)
        desc = tree.xpath('//pre/text()')

        if len(desc)>0:
            print(url)
            print(f'\n{clip}:')
            print(desc[0], flush=True)
        else:
            url= f'\n\nhttps://osc-mu-dev01.ontariosystems.com/csp/FACSDD/todd.DataDictionary.cls?global={clip}&field='
            r = requests.get(url = url, verify=False)
            tree = html.fromstring(r.content)
            desc = tree.xpath('//p/text()')
            if (len(desc)>0 and 'not defined' not in desc[0]):
                webbrowser.open(url)
            else:
                print(f'\n{clip}:')
                print('Term not found.', flush=True)

        app.minimize()
        app.restore()
        app.set_focus()
    
keyboard.add_hotkey('ctrl+shift+`', field_request)
keyboard.add_hotkey('ctrl+`', toggle_window)
keyboard.wait()