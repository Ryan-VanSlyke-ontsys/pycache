import testSuite as rt 
from ref import prompts, checkProcessByName as cp, jsonTools as jt # Customs

from types import SimpleNamespace
from pywinauto import Application
from pyautogui import press, typewrite, hotkey
import subprocess, os, time, json, pywinauto
import pyperclip

import warnings
warnings.simplefilter('ignore', category=UserWarning)

cp.check_pageant()

###
## Configuration
###
CONFIG = './ref/zConfig.txt'
NAMESPACEDIR = "C:\\InterSystems\\Cache\\CSP"
willTest = ''
namespace = ''
label = ''
labelString = ''
dirNum = 0

# Create a dictionary from JSON file as a SimpleNamspace so the fields can be referred to with dot notation
jsonFields = SimpleNamespace(**(jt.read_json_to_dict(CONFIG)))
# If the repository directory is empty, ask for one. Use the default if left blank.
if (jsonFields.RepositoryDirectory == ''):
    jsonFields.RepositoryDirectory = prompts.get_with_default('Enter your repository directory for the config (C:\\repos): ', 'C:\repos')
    # vars converts SimpleNameSpace back to a dict so it's JSON serializable
    jt.write_dict_to_json(vars(jsonFields),CONFIG)

# Get fields from json
jsonFields = SimpleNamespace(**(jt.read_json_to_dict(CONFIG)))
willTestDefault = jsonFields.WillTestDefault
defaultToBranch = jsonFields.DefaultToBranch
auto = jsonFields.Auto
lastNamespace = jsonFields.LastNamespace
# Get directory string using repr so the first char of the directory and the backslash aren't removed, replace the single quotes that repr creates,
# and make the single backslash a double backslash so it can be passed to the command line 
repositoryDirectory = repr(jsonFields.RepositoryDirectory).replace('\\','\\\\').replace("'",'')

# If automatic namespace creation is set to true, find an unused namespace and use it
if (auto.lower() == "true"):
    num = lastNamespace
    while (namespace == ''):
        namespace = f"FACS{str(num)}"
        for item in os.listdir(NAMESPACEDIR):
            if os.path.isdir(os.path.join(NAMESPACEDIR, item)):
                #print(item.lower() + " " + namespace.lower()) ## Watch comparisons
                dirNum += 1
                if (item.upper() == namespace.upper()): 
                   #print('Namespace exists in '+NAMESPACEDIR) ## Print when match is found
                    num+=1
                    namespace=''
                    break
    print(f'Namespace to create: {namespace}')
    # Increment LastNamespace and save to json
    jsonFields.LastNamespace = num
    jt.write_dict_to_json(vars(jsonFields),CONFIG)
else:
    # Automatic namespace creation is turned off. Ask for a namespace.
    while (namespace == ''):
        namespace = prompts.force_get("Enter a namespace: ").upper()
        # Check whether the entered namespace exists in the default directory. If it does, ask for another namespace.
        for item in os.listdir(NAMESPACEDIR):
            if os.path.isdir(os.path.join(NAMESPACEDIR, item)):
                dirNum += 1
                if (item.upper() == namespace):
                    confirm = input(f'Namespace exists in {NAMESPACEDIR}. Cloning will fail if repo is not empty. Continue? (N): ').upper()
                    if (confirm != "Y"):
                        namespace = ''
                    break

''' Studio section needs to account for studio opening the default namespace if there's only one namespace '''
# If studio opens with FACS1, do ... (I think we can assume if they're running the tool that it will be FACS1)
if (dirNum < 7): input('Please manually create another namespace before running this script'),exit()
# Get revision to clone
revision = prompts.get_with_default(f'Enter a branch to clone ({defaultToBranch}): ', defaultToBranch)

# Get Y or N for prompt to run tests
while (willTest != "Y" and willTest != "N"):
    willTest = prompts.get_with_default(f'Do you want to run the test suite? ({willTestDefault}): ', willTestDefault).upper()

label = prompts.get_with_default(f'Enter a label for your new branch, or blank to stay on the parent branch: ','')
if (label != ''):
    labelString = f' && hg branch {label}'

###
## Create namespace through the SQL method in the createNamespace cache script
###
application = Application().start(f'C:\\InterSystems\\Cache\\bin\\CTerm.exe /console=cn_iptcp:127.0.0.1[23] ./ref/createNamespace.scr {namespace}')
cterm = application.Cache_Telnet
cterm.wait('ready')

###
## Import cache mappings from file
###
studio = Application()
# Log into Studio
studio.start('C:\\InterSystems\\Cache\\bin\\CStudio.exe')
studio['Server Connection'].type_keys("{ENTER}")
studio['Log on to CACHE'].type_keys("SYS {ENTER}")
# Select namespace by typing it out in the connection manager
studio['Cache Connection Manager'].type_keys(namespace+"{ENTER}")
# Open import window
appName = f'CACHE/{namespace}@_SYSTEM - Default_system.prj - Studio'
studio[appName].type_keys('^i')
# Select mappings file
pyperclip.copy(f'{os.getcwd()}\\ref\\NSMappings.xml')
time.sleep(0.75)
studio['Open'].type_keys('+{INS}')
studio['Open'].type_keys('%O')
# Confirm in the import window and give Studio time to run the import
studio['Import'].type_keys('{ENTER}')
time.sleep(2)
studio.kill()

# Run mappings command in namespace using the cache script
cterm.type_keys("%{F4}") # Close previous CTerm
application = Application().start(f'C:\\InterSystems\\Cache\\bin\\CTerm.exe /console=cn_iptcp:127.0.0.1[23] ./ref/setFacsMappings.scr {namespace}')
cterm = application.Cache_Telnet
cterm.wait('ready')

###
## Clone the repo
###
start = time.time()
print(f'\nCloning {revision}: ')
CLONECOMMAND = f'cd {repositoryDirectory} && hg clone ssh://hg@bitbucket.org/ontsys/facs {namespace}'
CLONECOMMAND += f' && cd {namespace} && hg pull && hg update {revision}{labelString} && echo FACS >> .product && hg cache {namespace}'
# Run clone command in cmd. This will run hg cache.
result = os.system(CLONECOMMAND)
end = time.time()
runtime = end - start

###
## Run test suite when command returns 0 if tests should be run
###
if (result == 0):
    print(f'\nClone successful. Runtime: {runtime} seconds')
    if (willTest == "Y"):
        rt.run_tests(namespace)
else: print(f'\nClone failed with status: {str(result)} ')