from ref import prompts, jsonTools as jt # Customs
from pywinauto import Application
from types import SimpleNamespace
import pyperclip, pywinauto

import warnings
warnings.simplefilter('ignore', category=UserWarning)

def run_tests(namespace):
        Application().start(f'C:\\InterSystems\\Cache\\bin\\CTerm.exe /console=cn_iptcp:127.0.0.1[23] ./ref/runTests.scr {namespace}')

if __name__ == '__main__':

	CONFIG = "./ref/zConfig.txt"

	studioNamespace = None
	# Open CTerm and run the test command for the namespace and file to hit the break point in
	try:
		app = Application(backend='uia').connect(path='C:\\InterSystems\\Cache\\bin\\CStudio.exe')
		studio = app.top_window()
		studioWin = studio.element_info.name
		studioNamespace = studioWin[studioWin.find("/")+1:studioWin.find("@")]
	except pywinauto.application.ProcessNotFoundError as e:
		pass
	except Exception as e:
		print(e)

	if (studioNamespace != None):
		input = prompts.get_with_default(f'Run test suite for {studioNamespace}? (Y): ','Y')
		if (input.upper() == "Y"):
			run_tests(studioNamespace)
			quit()

	namespace = None
	while (namespace == None):
		namespace = prompts.force_get("Enter a namespace: ").upper()
		# Scan for available namespaces

	run_tests(namespace)